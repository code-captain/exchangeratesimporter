﻿using System.Threading.Tasks;
using RatesImporter.CryptoCurrency.Core.Models;

namespace RatesImporter.CryptoCurrency.Core.Contracts
{
    public interface IRemoteService<in TRequest, TResponse>
    {
        Task<OperationResult<TResponse>> Get(TRequest url);
    }
}
