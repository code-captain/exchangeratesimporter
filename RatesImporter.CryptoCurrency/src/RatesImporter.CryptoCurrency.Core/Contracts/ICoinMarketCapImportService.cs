﻿using RatesImporter.CryptoCurrency.Core.Models;

namespace RatesImporter.CryptoCurrency.Core.Contracts
{
    public interface ICoinMarketCapImportService : IRateImportService<RateImportRequestDto, RateImportDto>
    {
        
    }
}