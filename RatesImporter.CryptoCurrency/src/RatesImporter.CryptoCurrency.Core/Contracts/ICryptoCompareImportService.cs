﻿using RatesImporter.CryptoCurrency.Core.Models;

namespace RatesImporter.CryptoCurrency.Core.Contracts
{
    public interface ICryptoCompareImportService : IRateImportService<RateImportRequestDto, RateImportDto>
    {
        
    }
}