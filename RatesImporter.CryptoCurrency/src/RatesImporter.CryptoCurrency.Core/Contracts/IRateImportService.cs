﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RatesImporter.CryptoCurrency.Core.Models;

namespace RatesImporter.CryptoCurrency.Core.Contracts
{
    public interface IRateImportService<TRequest, TResponse>
    {
        Task<OperationResult<TRequest, TResponse>> GetRate(TRequest request);
        
        Task<List<OperationResult<TRequest, TResponse>>> GetRates(List<TRequest> requests);
    }
}