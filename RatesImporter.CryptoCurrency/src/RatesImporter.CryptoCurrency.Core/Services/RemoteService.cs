﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using RatesImporter.CryptoCurrency.Core.Contracts;
using RatesImporter.CryptoCurrency.Core.Models;

namespace RatesImporter.CryptoCurrency.Core.Services
{
    public class RemoteService : IRemoteService<string, string>
    {
        private readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        public async Task<OperationResult<string>> Get(string url)
        {
            if (String.IsNullOrWhiteSpace(url)) throw new ArgumentNullException(nameof(url));

            using (HttpClientHandler handler = new HttpClientHandler())
            {
                using (HttpClient httpClient = new HttpClient(handler))
                {
                    var response = await httpClient.GetAsync(url).ConfigureAwait(false);

                    if (response.IsSuccessStatusCode)
                    {
                        Logger.Info(url);
                        return new OperationResult<string>
                        {
                            IsSuccess = true,
                            RequestData = url,
                            ResponseData = await response.Content.ReadAsStringAsync()
                        };
                    }
                    
                    Logger.Error(string.Format("Error code {0} throwed by request {1}", response.StatusCode, url));
                    return new OperationResult<string>
                    {
                        IsSuccess = false,
                        RequestData = url
                    };
                }
            }
        }
    }
}
