﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using RatesImporter.CryptoCurrency.Core.Contracts;
using RatesImporter.CryptoCurrency.Core.Models;

namespace RatesImporter.CryptoCurrency.Core.Services
{
    public class AggregateRateImportService : IRateImportService<RateImportRequestDto, RateImportDto>
    {
        private readonly ICoinMarketCapImportService CoinMarketCapImportService;
        private readonly ICryptoCompareImportService CryptoCompareImportService;
        
        private readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        public AggregateRateImportService(
            ICoinMarketCapImportService coinMarketCapImportService, 
            ICryptoCompareImportService cryptoCompareImportService)
        {
            CoinMarketCapImportService = coinMarketCapImportService;
            CryptoCompareImportService = cryptoCompareImportService;
        }
        
        public async Task<OperationResult<RateImportRequestDto, RateImportDto>> GetRate(RateImportRequestDto importRequest)
        {
            return await FirstSuccessfulTask(
                ct => CryptoCompareImportService.GetRate(importRequest),
                ct => CoinMarketCapImportService.GetRate(importRequest));
        }

        public async Task<List<OperationResult<RateImportRequestDto, RateImportDto>>> GetRates(List<RateImportRequestDto> importConfigs)
        {
            var validateRequestResults = new List<OperationResult<RateImportRequestDto, RateImportDto>>();
            
            var requestResults = await CryptoCompareImportService.GetRates(importConfigs);
            foreach (var requestResult in requestResults)
            {
                if (!requestResult.IsSuccess)
                {
                    validateRequestResults.Add(
                        await FirstSuccessfulTask(
                            ct => CryptoCompareImportService.GetRate(requestResult.RequestData),
                            ct => CoinMarketCapImportService.GetRate(requestResult.RequestData)));
                }
                
                validateRequestResults.Add(requestResult);
            }

            return validateRequestResults;
        }

        private async Task<OperationResult<RateImportRequestDto, RateImportDto>> FirstSuccessfulTask(
            params Func<CancellationToken, Task<OperationResult<RateImportRequestDto, RateImportDto>>> [] functions)
        {
            var cts = new CancellationTokenSource();  
            var tasks = functions.Select(function => function(cts.Token)).ToArray();
            foreach (var task in tasks)
            {
                var completed = await task.ConfigureAwait(false);
                if (completed.IsSuccess)
                {
                    cts.Cancel();
                    return completed;
                }
            }

            return new OperationResult<RateImportRequestDto, RateImportDto>();
        }
    }
}