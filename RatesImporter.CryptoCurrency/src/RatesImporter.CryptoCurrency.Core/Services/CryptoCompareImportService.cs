﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Flurl;
using Newtonsoft.Json.Linq;
using RatesImporter.CryptoCurrency.Core.Contracts;
using RatesImporter.CryptoCurrency.Core.Models;

namespace RatesImporter.CryptoCurrency.Core.Services
{
    public class CryptoCompareImportService : ICryptoCompareImportService
    {
        private readonly IRemoteService<string, string> RemoteService;
        private readonly NLog.Logger Logger;
        
        private const string Url = "https://min-api.cryptocompare.com/data/pricemulti";
        
        
        public CryptoCompareImportService(IRemoteService<string, string> remoteService)
        {
            RemoteService = remoteService;
            Logger = NLog.LogManager.GetCurrentClassLogger();
        }
        
        
        public async Task<OperationResult<RateImportRequestDto, RateImportDto>> GetRate(RateImportRequestDto importRequest)
        {
            var requestUrl = Url
                .SetQueryParam("fsyms", importRequest.Symbol)
                .SetQueryParam("tsyms", "USD");
            var requestResult = await RemoteService.Get(requestUrl);
            
            if (!requestResult.IsSuccess)
                return new OperationResult<RateImportRequestDto, RateImportDto>
                {
                    IsSuccess = false,
                    RequestData = importRequest
                };
            
            try
            {
                return new OperationResult<RateImportRequestDto, RateImportDto>
                {
                    IsSuccess = true,
                    RequestData = importRequest,
                    ResponseData = CreateResponseDto(importRequest, requestResult.ResponseData)
                };
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Error by creating response dto");
                return new OperationResult<RateImportRequestDto, RateImportDto>
                {
                    IsSuccess = false,
                    RequestData = importRequest
                };
            }
        }

        public async Task<List<OperationResult<RateImportRequestDto, RateImportDto>>> GetRates(List<RateImportRequestDto> importConfigs)
        {
            var requestUrl = Url
                .SetQueryParam("fsyms", string.Join(",", importConfigs.Select(x => x.Symbol)))
                .SetQueryParam("tsyms", "USD");
            var requestResult = await RemoteService.Get(requestUrl);
            
            if (!requestResult.IsSuccess)
            {
                importConfigs.Select(importConfig => 
                    new OperationResult<RateImportRequestDto, RateImportDto>
                    {
                        IsSuccess = false,
                        RequestData = importConfig
                    });
            }

            return importConfigs
                .Select(importConfig =>
                    {
                        try
                        {
                            return new OperationResult<RateImportRequestDto, RateImportDto>
                            {
                                IsSuccess = true,
                                RequestData = importConfig,
                                ResponseData = CreateResponseDto(importConfig, requestResult.ResponseData)
                            };
                        }
                        catch (Exception ex)
                        {
                            Logger.Error(ex, "Error by creating response dto");
                            return new OperationResult<RateImportRequestDto, RateImportDto>
                            {
                                IsSuccess = false,
                                RequestData = importConfig
                            };
                        }
                    })
                .ToList();
        }
        
        private RateImportDto CreateResponseDto(RateImportRequestDto importRequest, string response)
        {
            if (string.IsNullOrEmpty(response)) 
                throw new ArgumentNullException(nameof(response));
            
            var jsonResponse = JObject.Parse(response);
            var jsonObject = jsonResponse[importRequest.Symbol];
            
            return new RateImportDto
            {
                Price = decimal.Parse((string)jsonObject["USD"], CultureInfo.InvariantCulture)
            };
        }
    }
}