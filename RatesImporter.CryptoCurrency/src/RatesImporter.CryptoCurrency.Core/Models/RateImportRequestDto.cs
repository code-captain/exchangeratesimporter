﻿namespace RatesImporter.CryptoCurrency.Core.Models
{
    public class RateImportRequestDto
    {
        public string Urn { get; set; }
        
        public string Name { get; set; }

        public string Symbol { get; set; }
    }
}