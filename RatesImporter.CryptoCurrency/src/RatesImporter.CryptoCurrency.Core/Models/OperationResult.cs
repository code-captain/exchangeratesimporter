﻿namespace RatesImporter.CryptoCurrency.Core.Models
{
    public class OperationResult<T>
    {
        public bool IsSuccess { get; set; }

        public T RequestData { get; set; }

        public T ResponseData { get; set; }
    }
    
    public class OperationResult<TRequest, TResponse>
    {
        public bool IsSuccess { get; set; }

        public TRequest RequestData { get; set; }

        public TResponse ResponseData { get; set; }
    }
}
