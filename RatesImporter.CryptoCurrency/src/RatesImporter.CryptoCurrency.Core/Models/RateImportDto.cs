﻿namespace RatesImporter.CryptoCurrency.Core.Models
{
    public class RateImportDto
    {
        public decimal Price { get; set; }
    }
}
