﻿namespace RatesImporter.CryptoCurrency.Core.Constants
{
    public static class UrlAdresses
    {
        public const string CryptoComparePriceUrl = "https://min-api.cryptocompare.com/data/";

        public const string CoinMarketCapUrl = "https://api.coinmarketcap.com/v1/ticker/";
    }
}
