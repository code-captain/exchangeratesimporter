﻿using System.Collections.Generic;
using RatesImporter.CryptoCurrency.Core.Models;

namespace RatesImporter.CryptoCurrency.Core.Constants
{
    public static class RateImportRequests
    {
        public static List<RateImportRequestDto> Requests { get; private set; } =
            new List<RateImportRequestDto>()
            {
                new RateImportRequestDto
                {
                    Name = "Bitcoin",
                    Urn = "bitcoin",
                    Symbol = "BTC"
                },
            
                new RateImportRequestDto
                {
                    Name = "BitcoinCash",
                    Urn = "bitcoin-cash",
                    Symbol = "BCH"
                },

                new RateImportRequestDto
                {
                    Name = "Ethereum",
                    Urn = "ethereum",
                    Symbol = "ETH"
                },

                new RateImportRequestDto
                {
                    Name = "Neo",
                    Urn = "neo",
                    Symbol = "NEO"
                },
            
                new RateImportRequestDto
                {
                    Name = "Litecoin",
                    Urn = "litecoin",
                    Symbol = "LTC"
                },
            
                 new RateImportRequestDto
                 {
                     Name = "Qtum",
                     Urn = "qtum",
                     Symbol = "QTUM"
                 },
             
                new RateImportRequestDto
                {
                    Name = "Cardano",
                    Urn = "cardano",
                    Symbol = "ADA"
                },

                new RateImportRequestDto
                {
                    Name = "Dash",
                    Urn = "dash",
                    Symbol = "DASH"
                },
            
                new RateImportRequestDto
                {
                    Name = "Monero",
                    Urn = "monero",
                    Symbol = "XMR"
                },
             
                new RateImportRequestDto
                {
                    Name = "Stellar",
                    Urn = "stellar",
                    Symbol = "XLM"
                },
            
                new RateImportRequestDto
                {
                    Name = "Zcash",
                    Urn = "zcash",
                    Symbol = "ZEC"
                },
            
                new RateImportRequestDto
                {
                    Name = "Iota",
                    Urn = "iota",
                    Symbol = "IOTA"
                },
            
                new RateImportRequestDto
                {
                    Name = "Waves",
                    Urn = "waves",
                    Symbol = "WAVES"
                },

                new RateImportRequestDto
                {
                    Name = "Nem",
                    Urn = "nem",
                    Symbol = "XEM"
                },
             
                new RateImportRequestDto
                {
                    Name = "BitShares",
                    Urn = "bitshares",
                    Symbol = "BTS"
                }
            };
    }
}