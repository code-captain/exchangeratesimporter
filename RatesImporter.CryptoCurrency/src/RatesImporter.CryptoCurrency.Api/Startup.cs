﻿using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;
using NLog.Web;
using RatesImporter.CryptoCurrency.Core.Contracts;
using RatesImporter.CryptoCurrency.Core.Models;
using RatesImporter.CryptoCurrency.Core.Services;

namespace RatesImporter.CryptoCurrency.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //business-logic services
            services.AddTransient<IRemoteService<string, string>, RemoteService>();
            services.AddTransient<ICryptoCompareImportService, CryptoCompareImportService>();
            services.AddTransient<ICoinMarketCapImportService, CoinMarketCapImportService>();
            services.AddTransient<IRateImportService<RateImportRequestDto, RateImportDto>, AggregateRateImportService>();
            
            services.AddMvc();
            services.AddAutoMapper();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddNLog();

            env.ConfigureNLog("Nlog.config");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
            app.AddNLogWeb();
        }
    }
}
