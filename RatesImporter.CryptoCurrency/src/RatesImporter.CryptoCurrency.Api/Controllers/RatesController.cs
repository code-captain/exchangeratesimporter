﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RatesImporter.CryptoCurrency.Core.Constants;
using RatesImporter.CryptoCurrency.Core.Contracts;
using RatesImporter.CryptoCurrency.Core.Models;

namespace RatesImporter.CryptoCurrency.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/rates")]
    public class RatesController : Controller
    {
        private readonly IRateImportService<RateImportRequestDto, RateImportDto> RatesImportService;

        public RatesController(IRateImportService<RateImportRequestDto, RateImportDto> ratesImportService)
        {
            RatesImportService = ratesImportService;
        }

        // GET api/rates
        [HttpGet]
        public async Task<JsonResult> Get()
        {
            var operationResults = await RatesImportService.GetRates(RateImportRequests.Requests);
            var rates = operationResults.Select(operationResult => new
            {
                operationResult.RequestData.Name,
                operationResult.ResponseData?.Price
            });

            return Json(rates);
        }
    }
}